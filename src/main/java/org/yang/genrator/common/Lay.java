package org.yang.genrator.common;

import lombok.Builder;
import lombok.Data;

/**
 * @author shichenyang
 */
@Data
@Builder
public class Lay {
    private Integer limit;

    private Integer page;

    private int code;

    private String msg;

    private Long count;

    private Object data;
}
