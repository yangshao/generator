package org.yang.genrator.common;

import lombok.Builder;
import lombok.Data;

/**
 * 当添加时，或修改时，返回的信息
 */
@Data
@Builder
public class Msg {
    //返回编码，0是成功
    private int code;
    //提示消息
    private String msg;
    //返回数据
    private Object data;



}
