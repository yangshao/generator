package org.yang.genrator.service;

import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.yang.genrator.code.Code;
import org.yang.genrator.common.Lay;
import org.yang.genrator.dao.GeneratorDao;
import org.yang.genrator.entity.Column;
import org.yang.genrator.entity.Table;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shichenyang
 */
@Service
@Transactional(rollbackFor = Exception.class)
@CommonsLog
public class GeneratorService {
    @Resource
    private GeneratorDao generatorDao;

    public Lay getByList(String tableName,Integer page,Integer limit){
        List<Table> list=generatorDao.getListTable(tableName);
        Lay lay=Lay.builder().count(Long.valueOf(list.size())).data(list).build();
        return lay;
    }

    public List<Column> getByColName(String tableName){
        Configuration configuration= Code.getConfig();
        List<Column> columns=generatorDao.getListColumn(tableName);
        for (Column c:columns){
            c.setType(configuration.getString(c.getDataType()));
            c.setProName(Code.columnToJava(c.getColName()));
            c.setLiteName(StringUtils.uncapitalize(c.getProName()));
        }
        return columns;
    }

    public Table getOne(String tableName){
        Table table=generatorDao.getOne(tableName);
        table.setEntityName(Code.tableToJava(tableName,""));
        table.setLiteName(StringUtils.uncapitalize(table.getEntityName()));
        return table;
    }
}
