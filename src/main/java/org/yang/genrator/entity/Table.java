package org.yang.genrator.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author shichenyang
 */
@Data
@Builder
public class Table {
    //数据中表名
    private String tableName;
    //实体类名
    private String entityName;
    private String liteName;//
    private String comment;//表注释
    private String engine;//存储引擎
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    private List<Column> columns;
}
