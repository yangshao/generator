package org.yang.genrator.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Column {
    private String colName;//数据库中列名
    private String proName;//属性名称 大写
    private String liteName;//属性名称 小写
    private String dataType ;//数据库中类型
    private String type;//java 数据类型
    private String colComment;//字段注释
}
