package org.yang.genrator.dao;

import org.apache.ibatis.annotations.Param;
import org.yang.genrator.entity.Column;
import org.yang.genrator.entity.Table;

import java.util.List;

/**
 * @author shichenyang
 */
public interface GeneratorDao {
     /**
      * 获取表
      * @param tableName
      * @return
      */
     List<Table> getListTable(@Param("tableName")String tableName);

     /**
      * 根据表格名称获取
      * @param tableName
      * @return
      */
     Table getOne(@Param("tableName")String tableName);

     /**
      * 根据表名获取所有的字段
      * @param tableName
      * @return
      */
     List<Column> getListColumn(@Param("tableName") String tableName);
}
