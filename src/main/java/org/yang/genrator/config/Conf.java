package org.yang.genrator.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author shichenyang
 */
@Data
@Component
@ConfigurationProperties("code")
public class Conf {
    private String mainPath;
    private String author;
    private String email;
    private String moduleName;

}
