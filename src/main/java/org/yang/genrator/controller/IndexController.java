package org.yang.genrator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author shichenyang
 */
@Controller
public class IndexController {
    /**
     * 返回主页
     * @return 主页
     */
    @RequestMapping("/")
    public String index(){
        return "index.html";
    }
}
