# generator
<a href='https://gitee.com/yangshao/generator/stargazers'><img src='https://gitee.com/yangshao/generator/badge/star.svg?theme=dark' alt='star'></img></a>

#### 项目介绍
做了一套代码生成自用

#### 软件架构
很简单的spring boot + beetl +layui 


#### 安装教程

1. 使用git clone https://gitee.com/yangshao/generator.git 命令将项目进行下载
2. 修改 application.yml中的数据源,项目会以连接的数据源加载表信息.同时修改以code开头的配置信息，改成你生成项目的包名、模块名、作者、邮箱
3. 修改完毕运行 ,打开 http://localhost
4. 在项目中使用的beetl模板引擎
5. 模板部分按照阿里巴巴JAVA规范进行了注释上的调整
6. 项目目前自用足够，页面没有写的太复杂。但是说实话，自用的话改成插件形式是最好的


